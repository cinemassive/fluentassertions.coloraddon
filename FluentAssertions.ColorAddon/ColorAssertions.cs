﻿using System;
using System.Drawing;
using FluentAssertions.Execution;
using FluentAssertions.Primitives;

namespace FluentAssertions
{
    public class ColorAssertions : ReferenceTypeAssertions<Color, ColorAssertions>
    {
        public ColorAssertions(Color subject)
        {
            Subject = subject;
        }

        protected override string Context
        {
            get { return Subject.GetType().Name; }
        }

        public AndConstraint<ColorAssertions> Be(Color expectedColor, string reason = "", params object[] reasonArgs)
        {
            Execute.Assertion
                    .ForCondition(  AboutEquals(Subject.A, expectedColor.A, 1) 
                                 && AboutEquals(Subject.R, expectedColor.R, 1)
                                 && AboutEquals(Subject.G, expectedColor.G, 1) 
                                 && AboutEquals(Subject.B, expectedColor.B, 1))
                    .BecauseOf(reason, reasonArgs)
                    .FailWith("Expected ARGB values of {0}, but found {1}.",
                            Color.FromArgb(expectedColor.A, expectedColor.R, expectedColor.G, expectedColor.B),
                            Color.FromArgb(Subject.A, Subject.R, Subject.G, Subject.B)
                    );

            return new AndConstraint<ColorAssertions>(this);
        }


        public AndConstraint<ColorAssertions> Be(byte alpha, byte red, byte green, byte blue, string reason = "",
                params object[] reasonArgs)
        {
            return Be(Color.FromArgb(alpha, red, green, blue), reason, reasonArgs);
        }

        public AndConstraint<ColorAssertions> Be(byte red, byte green, byte blue, string reason = "",
                params object[] reasonArgs)
        {
            return Be(Color.FromArgb(red, green, blue), reason, reasonArgs);
        }

        private bool AboutEquals(int a, int b, int maxDelta)
        {
            return Math.Abs(a - b) <= maxDelta;
        }
    }
}