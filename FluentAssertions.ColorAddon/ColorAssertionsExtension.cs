﻿using System.Drawing;

namespace FluentAssertions
{
    public static class ColorAssertionsExtension
    {
        public static ColorAssertions Should(this Color color)
        {
            return new ColorAssertions(color);
        }
    }
}