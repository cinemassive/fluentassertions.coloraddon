﻿using System;
using System.Diagnostics;
using System.Drawing;
using FluentAssertions.Execution;
using NUnit.Framework;

namespace FluentAssertions.ColorAddon.Tests
{
    [TestFixture]
    public class ColorComparisonTests
    {
        [Test]
        public void RuntimeColorEqualsPremadeColor()
        {
            Color.Red.Should().Be(Color.FromArgb(255, 255, 0, 0));
            Color.FromArgb(255, 0, 0, 255).Should().Be(Color.Blue);
        }

        [Test]
        public void BeAllowsColorsToBeOffByOne()
        {
            Color.Red.Should().Be(Color.FromArgb(254, 254, 0, 0));
            Color.FromArgb(127, 63, 12, 251).Should().Be(Color.FromArgb(128, 64, 13, 250));
        }

        [Test]
        public void BeAllowsRGBComparisonWithoutWrapper()
        {
            Color.Red.Should().Be(255, 0, 0);
            Color.FromArgb(0, 128, 0).Should().Be(0, 127, 0);
        }

        [Test]
        public void BeAllowsARGBComparisonWithoutWrapper()
        {
            Color.Gray.Should().Be(255, 127, 127, 127);
            Color.FromArgb(1, 2, 3, 4).Should().Be(2, 3, 3, 3);
        }

        [Test]
        [ExpectedException(typeof(AssertionException))]
        public void TransparentIsNotDodgerBlue()
        {
            Color.Transparent.Should().Be(Color.DodgerBlue);
        }
    }
}